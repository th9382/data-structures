require 'queue_linked_list'

describe Queue do
  before do
    @queue = Queue.new
  end

  it "initializes with first and last set to nil" do
    expect(@queue.instance_variable_get(:@first)).to be(nil)
    expect(@queue.instance_variable_get(:@last)).to be(nil)
    expect(@queue.length).to be(0)
  end

  it "has enqueue and dequeue operation" do
    @queue.enqueue(1)
    @queue.enqueue(nil)
    expect(@queue.dequeue).to be(1)
    expect(@queue.dequeue).to be(nil)
    expect(@queue.length).to be(0)
  end

  it "can do many enqueue and dequeue operations" do
    10.times { |i| @queue.enqueue(i) }

    0.upto(9).each do |i|
      expect(@queue.dequeue).to be(i)
      expect(@queue.length).to be(9 - i)
    end
  end

  it "raises error when trying to dequeue from empty queue" do
    expect { @queue.dequeue }.to raise_error("Can't dequeue from empty list")
  end
end
