require 'dynamic_array'

describe DynamicArray do
  before do
    @arr = DynamicArray.new
  end

  it "initializes with capacity 8" do
    expect(@arr.instance_variable_get(:@capacity)).to be(8)
  end

  it "initializes with length 0" do
    expect(@arr.length).to be(0)
  end

  it "can index into array" do
    @arr.push(9)
    expect(@arr[0]).to be(9)
  end

  it "can set value for index" do
    @arr.push(9)
    @arr[0] = 1
    expect(@arr[0]).to be(1)
  end

  it "has push operation" do
    @arr.push(9)
    @arr.push(8)

    expect(@arr[0]).to be(9)
    expect(@arr[1]).to be(8)
    expect(@arr.length).to be(2)
  end

  it "has pop operation" do
    @arr.push(9)
    @arr.push(8)
    expect(@arr.length).to be(2)
    expect(@arr.pop).to be(8)
    expect(@arr.length).to be(1)
    expect(@arr.pop).to be(9)
    expect(@arr.length).to be(0)
  end

  it "has unshift operation" do
    @arr.unshift(1)
    @arr.unshift(2)
    expect(@arr[0]).to be(2)
    expect(@arr[1]).to be(1)
  end

  it "has shift operation" do
    @arr.unshift(1)
    @arr.unshift(2)
    expect(@arr.shift).to be(2)
    expect(@arr.shift).to be(1)
    expect{ @arr.shift }.to raise_error("can't shift when array is empty")
  end

  it "raises error if index is out of bounds" do
    expect { @arr[2] }.to raise_error("invalid index")
  end

  it "resizes array by doubling capacity" do
    8.times { @arr.push(1) }
    expect(@arr.instance_variable_get(:@capacity)).to be(8)
    @arr.push(1)
    expect(@arr.instance_variable_get(:@capacity)).to be(16)
    9.times do |i|
      expect(@arr[i]).to be(1)
    end
  end
end
