require "hashmap"

describe Hashmap do
  before do
    @hash = Hashmap.new
  end

  it "returns nil if key is not in Hashmap" do
    expect(@hash["random"]).to be(nil)
  end

  it "saves key in hashmap" do
    @hash["test"] = "result"
    expect(@hash["test"]).to eq("result")
  end

  it "deletes a key" do
    @hash["test"] = "result"
    @hash.delete("test")
    expect(@hash["test"]).to be(nil)
  end

  it "supports setting and getting many keys" do
    100.times { |i| @hash[i]="key#{i}" }
    100.times { |i| expect(@hash[i]).to eq("key#{i}") }
  end

  it "resizes array to keep load factor < 1" do
    50.times { |i| @hash[i]="key#{i}" }
    num_buckets = @hash.instance_variable_get(:@buckets).length
    expect(@hash.instance_variable_get(:@count).fdiv(num_buckets)).to be < 1
  end

  it "overwrites duplicate key entries" do
    @hash["test"] = "first"
    @hash["test"] = "second"
    expect(@hash["test"]).to eq("second")
  end
end
