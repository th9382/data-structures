require "static_array"

describe StaticArray do
  before do
    @arr = StaticArray.new(4)
  end

  it "initializes with correct length" do
    expect(@arr.length).to be(4)
  end

  it "can index into array" do
    expect(@arr[0]).to be(nil)
  end

  it "correctly sets value at index" do
    @arr[0] = 9
    expect(@arr[0]).to be(9)
  end

  it "raises error if index out of range" do
    expect{ @arr[10] }.to raise_error(ArgumentError)
  end
end
