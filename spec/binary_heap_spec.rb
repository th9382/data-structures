require "binary_heap"

describe BinaryHeap do
  before do
    @bh = BinaryHeap.new
  end

  it "raises error if key is not numeric" do
    expect { @bh.insert("a", 1) }.to raise_error("Key must be numeric!")
  end

  it "inserts item" do
    @bh.insert(10, 1)
    expect(@bh.top).to be(1)
  end

  it "correctly inserts multiple items" do
    @bh.insert(10, 1)
    @bh.insert(12, 2)
    @bh.insert(7, 3)

    expect(@bh.top).to be(3)
  end

  it "removes min key" do
    @bh.insert(12, 2)
    @bh.insert(7, 3)
    expect(@bh.remove).to be(3)
    expect(@bh.remove).to be(2)
  end
end
