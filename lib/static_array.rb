class StaticArray
  def initialize(length)
    @store = Array.new(length, nil)
  end

  def [](index)
    check_index(index)
    store[index]
  end

  def []=(index, value)
    check_index(index)
    store[index] = value
  end

  def length
    store.length
  end

  private

  def check_index(index)
    if index < 0 || index >= length
      raise ArgumentError.new("Index out of range")
    end
  end

  protected
  attr_accessor :store
end
