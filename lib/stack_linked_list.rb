require_relative 'node'

class Stack
  def initialize
    @first = nil
  end

  def empty?
    first.nil?
  end

  def push(value)
    self.first = Node.new(value, first)

    nil
  end

  def pop
    raise "Can't pop from empty list" if empty?

    old_first = first
    self.first = first.next

    old_first.value
  end

  protected
  
  attr_accessor :first
end
