require_relative 'static_array'

class RingBuffer
  attr_reader :length

  def initialize
    @store = StaticArray.new(8)
    @capacity = 8
    @length = 0
    @start_index = 0
  end

  def [](index)
    check_index(index)
    store[(index + start_index) % capacity]
  end

  def []=(index, value)
    check_index(index)
    store[(index + start_index) % capacity] = value
  end

  def push(value)
    resize! if length == capacity

    self.length += 1
    self[length - 1] = value

    nil
  end

  def pop
    raise "can't pop from empty array" unless length > 0

    value = self[length - 1]
    self[length - 1] = nil
    self.length -= 1

    value
  end

  def unshift(value)
    resize! if length == capacity

    self.start_index = (start_index - 1) % capacity

    self.length += 1
    self[0] = value

    nil
  end

  def shift
    raise "can't shift from empty array" unless length > 0

    value = self[0]
    self[0] = nil
    self.start_index = (start_index + 1) % capacity
    self.length -= 1

    value
  end

  protected
  attr_accessor :store, :capacity, :start_index
  attr_writer :length

  def check_index(index)
    unless (index >= 0) && (index < length)
      raise "invalid index"
    end
  end

  def resize!
    new_capacity = capacity * 2
    new_store = StaticArray.new(new_capacity)

    length.times { |i| new_store[i] = self[i] }

    self.store = new_store
    self.capacity = new_capacity
    self.start_index = 0
  end
end
