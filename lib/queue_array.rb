require_relative 'ring_buffer'

class Queue
  def initialize
    @store = RingBuffer.new
  end

  def enqueue(value)
    store.push(value)
  end

  def dequeue
    store.shift
  end

  private

  attr_reader :store
end
