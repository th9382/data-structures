class Node
  attr_accessor :value, :next

  def initialize(value = nil, next_node = nil)
    @value, @next = value, next_node
  end
end
