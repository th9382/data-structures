require_relative 'node'

class SList
  attr_reader :size, :head

  def initialize
    @head = nil
    @size = 0
  end

  def [](index)
    check_index(index)
    current_node = head
    index.times { current_node = current_node.next }

    current_node.value
  end

  def insert_front(value)
    # O(1)
    old_head = head
    @head = Node.new(value, old_head)
    @size += 1

    value
  end

  def insert_end(value)
    if empty?
      @head = Node.new(value, nil)
    else
      current_node = head
      until current_node.next.nil?
        current_node = current_node.next
      end

      current_node.next = Node.new(value, nil)
    end

    @size += 1

    value
  end

  def empty?
    head.nil?
  end

  def remove(value) # only removes first value it finds
    # O(n)
    return nil if empty?

    if head.value == value && !empty?
      @head = head.next
      @size -= 1
      return value
    else
      current_node = head

      until current_node.next.next.nil?
        if current_node.next.value == value
          current_node.next = current_node.next.next
          @size -= 1
          return value
        end

        current_node = current_node.next
      end
    end

    nil
  end

  def remove_all(value)
    # O(n)
    return nil if empty?

    match = false

    while !empty? && head.value == value
      @head = head.next
      @size -= 1
      match = true
    end

    current_node = head

    until current_node.nil? || current_node.next.nil?
      if current_node.next.value == value
        current_node.next = current_node.next.next
        @size -= 1
        match = true
        next
      end

      current_node = current_node.next
    end

    match ? value : nil
  end

  protected

  def check_index(index)
    unless (index >= 0) && (index < size) && !empty?
      raise "invalid index"
    end
  end

  attr_writer :head
end
